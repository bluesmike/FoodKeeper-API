# FoodKeeper API
![version](https://img.shields.io/badge/version-1.2.0-green.svg?cacheSeconds=2592000)

API for USDA FSIS FoodKeeper data (<https://www.fsis.usda.gov/shared/data/EN/foodkeeper.json>)

## Documentation

- Please see our [documentation on librefoodpantry.org](https://librefoodpantry.org/#/projects/FoodKeeper-API/)

- For example API calls please see [the FoodKeeper API](https://foodkeeper-api.herokuapp.com/) 

- The source for our documentation is in [/docs](docs)
  


---
Copyright &copy; 2019 The LibreFoodPantry Community. This work is licensed
under the Creative Commons Attribution-ShareAlike 4.0 International License.
To view a copy of this license, visit
http://creativecommons.org/licenses/by-sa/4.0/.
