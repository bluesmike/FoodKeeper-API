package com.gitlab.LibreFoodPantry.FoodKeeperAPI.Models;

import java.util.Map;

import com.gitlab.LibreFoodPantry.FoodKeeperAPI.JsonConverter;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "Class representing a category tracked by the application")
public class Category {
	@ApiModelProperty(notes = "Unique identifier for the category", example = "1", required = true, position = 0)
	private Integer id;
	@ApiModelProperty(notes = "Name of the category", example = "Meat", required = true, position = 1)
	private String name;
	@ApiModelProperty(notes = "Name of the category's subcategory (if it exists)", example = "Fresh", position = 2)
	private String subcategory;
	
	public Category() {}
	
	public Category(Integer id, String name, String subcategory) {
		this.id = id;
		this.name = name;
		this.subcategory = subcategory;
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSubcategory() {
		return subcategory;
	}

	public void setSubcategory(String subcategory) {
		this.subcategory = subcategory;
	}

	@Override
	public String toString() {
		return "Category [id=" + id + ", name=" + name + ", subcategory=" + subcategory + "]";
	}
	
	public void overwrite(Category category) {
		this.name = category.name != null ? category.name : this.name;
		this.subcategory = category.subcategory != null ? category.subcategory : this.subcategory;
	}
	
	public static Category getFromJsonArray(JsonArray jsonArr) {
		Map<String, JsonElement> map = JsonConverter.getMapFromJsonArray(jsonArr);
		Category newCategory = new Category();

		newCategory.id = JsonConverter.getJsonElementAsInt(map.get("ID"));
		newCategory.name = JsonConverter.getJsonElementAsString(map.get("Category_Name"));
		newCategory.subcategory = JsonConverter.getJsonElementAsString(map.get("Subcategory_Name"));
		
		return newCategory;
	}
}
